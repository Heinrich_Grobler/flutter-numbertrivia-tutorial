import 'package:clean_arch_numbertrivia/Features/number_trivia/data/datasources/number_trivia_remote_datasource.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/data/models/number_trivia_model.dart';
import 'package:clean_arch_numbertrivia/core/error/exceptions.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../Fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  NumberTriviaRemoteDataSourceImpl datasource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    datasource = NumberTriviaRemoteDataSourceImpl(client: mockHttpClient);
  });
  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response(fixture('trivia.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('something went wrong', 404));
  }

  group('getConcereteNumberTrivia', () {
    final tNumber = 1;

    test(
        'should perform a get request with the number being the endpoint and head being type json',
        () async {
      //arrange
      setUpMockHttpClientSuccess200();
      // act
      datasource.getConcreteNumberTrivia(tNumber);
      // assert
      verify(mockHttpClient.get('http://numberapi.com/$tNumber',
          headers: {'Content-Type': 'application/json'}));
    });

    final tNumberTriviaModel =
        NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));

    test('should return numbertrivia when the response code is 200', () async {
      //arrange
      setUpMockHttpClientSuccess200();
      // act
      final result = await datasource.getConcreteNumberTrivia(tNumber);
      // assert

      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw a serverException when the repsonse code is not 200 ', () async {
      //arrange
      setUpMockHttpClientFailure404();
      // act
final call = datasource.getConcreteNumberTrivia;
      // assert
      expect(() => call(tNumber), throwsA(isInstanceOf<ServerException>()));
    });
  });

  group('getRandomNumberTrivia', () {


    test(
        'should perform a get request with random being the endpoint and head being type json',
            () async {
          //arrange
          setUpMockHttpClientSuccess200();
          // act
          datasource.getRandomNumberTrivia();
          // assert
          verify(mockHttpClient.get('http://numberapi.com/random',
              headers: {'Content-Type': 'application/json'}));
        });

    final tNumberTriviaModel =
    NumberTriviaModel.fromJson(json.decode(fixture('trivia.json')));

    test('should return numbertrivia when the response code is 200', () async {
      //arrange
      setUpMockHttpClientSuccess200();
      // act
      final result = await datasource.getRandomNumberTrivia();
      // assert

      expect(result, equals(tNumberTriviaModel));
    });

    test('should throw a serverException when the repsonse code is not 200 ', () async {
      //arrange
      setUpMockHttpClientFailure404();
      // act
      final call = datasource.getRandomNumberTrivia;
      // assert
      expect(() => call(), throwsA(isInstanceOf<ServerException>()));
    });
  });
}


