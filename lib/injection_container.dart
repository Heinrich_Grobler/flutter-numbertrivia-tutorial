import 'package:clean_arch_numbertrivia/Features/number_trivia/data/datasources/number_trivia_local_datasource.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/data/datasources/number_trivia_remote_datasource.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/domain/usecases/get_random_number_trivia.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/presentation/bloc/bloc.dart';
import 'package:clean_arch_numbertrivia/core/network/network_info.dart';
import 'package:clean_arch_numbertrivia/core/util/input_converter.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'Features/number_trivia/data/repositories/number_trivia_repositoryImpl.dart';
import 'Features/number_trivia/domain/repositories/number_trivia_repository.dart';

final sl = GetIt.instance;
Future<void> init() async {
  // features
  sl.registerFactory(() =>
      NumberTriviaBloc(concrete: sl(), inputConverter: sl(), random: sl()));

  sl.registerLazySingleton(() => GetConcreteNumberTrivia(sl()));
  sl.registerLazySingleton(() => GetRandomNumberTrivia(sl()));

  sl.registerLazySingleton<NumberTriviaRepository>(
    () => NumberTriviaRepositoryImpl(
      remoteDatasource: sl(),
      localDatasource: sl(),
      networkInfo: sl(),
    ),
  );
  sl.registerLazySingleton<NumberTriviaLocalDataSource>(
    () => NumberTriviaLocalDataSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<NumberTriviaRemoteDataSource>(
      () => NumberTriviaRemoteDataSourceImpl(client: sl()));

  // core
  sl.registerLazySingleton(() => InputConverter());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  // External
  WidgetsFlutterBinding.ensureInitialized();

 final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
