import 'package:clean_arch_numbertrivia/Features/number_trivia/domain/entities/number_trivia.dart';
import 'package:clean_arch_numbertrivia/Features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:clean_arch_numbertrivia/core/error/failures.dart';
import 'package:clean_arch_numbertrivia/core/usecases/usecase.dart';
import 'package:dartz/dartz.dart';

class GetRandomNumberTrivia implements UseCase<NumberTrivia,NoParams>{

  final NumberTriviaRepository repository;

  GetRandomNumberTrivia(this.repository);

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams params) async{
    return await repository.getRandomNumberTrivia();
  }
}

